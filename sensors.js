// sensors.js
module.exports = {
	getSoilHumidity: function () {
		return sh;
	}
};

// ADS1x15
"use strict";
var Raspi = require('raspi');
var I2C = require('raspi-i2c').I2C;
var ADS1x15 = require('raspi-kit-ads1x15');
var Async = require('async');

var sh;

var utils = require('./utils.js');

Raspi.init(() => {
	var i2c = new I2C();
	var adc = new ADS1x15({
		i2c,                                    // i2c interface
		chip: ADS1x15.chips.IC_ADS1015,         // chip model
		address: ADS1x15.address.ADDRESS_0x48,  // i2c address on the bus
		pga: ADS1x15.pga.PGA_4_096V,            // power-gain-amplifier range
		sps: ADS1x15.spsADS1015.SPS_250         // data rate (samples per second)
	});

	let done = false;
	// Graceful shutdown
	process.once('SIGTERM', () => done = true);
	process.once('SIGINT', () => done = true);

	// A0
	adc.startContinuousChannel(ADS1x15.channel.CHANNEL_3, (err, value, volts) => {
		if (err) {
			console.error('Failed to fetch value from ADC', err);
		} else {
			sh = utils.mapPercentage(value, 1950, 550);
		}

		setTimeout(() => {
			Async.whilst(() => done === false,
				(nextReading) => {
					adc.getLastReading((err, value, volts) => {
						if (err) {
							console.error('Failed to fetch value from ADC', err);
						} else {
							sh = utils.mapPercentage(value, 1950, 550);
						}
						setTimeout(() => nextReading(err), 500);
					});
				},
				(/*err*/) => {
					adc.stopContinuousReadings((/*err*/) => {
						// DONE!
						console.log('\nFinished readings.\n');
					});
				}
			);
		}, 500);

	});

});