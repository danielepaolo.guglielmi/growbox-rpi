// utils.js
module.exports = {
	mapPercentage: function (x, min, max) {
		return (((x - min) * 100) / (max - min)).toFixed(0);
	}
};
