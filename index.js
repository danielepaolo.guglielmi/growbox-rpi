var Blynk = require('blynk-library');

var AUTH = 'fa1c690065784978b22116d94a71a9e7';

var blynk = new Blynk.Blynk(AUTH);
var Gpio = require('onoff').Gpio

blynk.on('error', (err) => {
	console.error(err);

});

var cpu = new blynk.VirtualPin(10);
var ram = new blynk.VirtualPin(11);

cpu.on('read', function () {
	var exec = require('child_process').exec;
	exec('top -b -d1 -n1|grep -i "Cpu(s)"|head -c21|cut -d " " -f2|cut -d "%" -f1',
			function callback(error, stdout, stderr) {
		if (error)
			cpu.write(null);
		else
			cpu.write(parseFloat(stdout, 10).toFixed(1));
	});
});

ram.on('read', function () {
	var exec = require('child_process').exec;
	exec('free | grep Mem | awk \'{print $3/$2 * 100.0}\'',
			function callback(error, stdout, stderr) {
		if (error)
			cpu.write(null);
		else
			ram.write(parseFloat(stdout, 10).toFixed(1));
	});
});

// SENSORS
var sensors = require('./sensors.js');
var rpiDhtSensor = require('rpi-dht-sensor');
var dhtSensor = new rpiDhtSensor.DHT11(18);
var airTemperature;
var vAirTemperature = new blynk.VirtualPin(0);
var airHumidity;
var vAirHumidity = new blynk.VirtualPin(1);
function getDHT11Vals() {
	var x = dhtSensor.read();
	airTemperature = x.temperature.toFixed(0);
	airHumidity = x.humidity.toFixed(0);
}
vAirTemperature.on('read', function () {
	vAirTemperature.write(airTemperature);
});
vAirHumidity.on('read', function () {
	vAirHumidity.write(airHumidity);
});
var soilHumidity;
var vSoilHumidity = new blynk.VirtualPin(2);
function getSoilHumidity() {
	soilHumidity = Number(sensors.getSoilHumidity());
}
vSoilHumidity.on('read', function () {
	vSoilHumidity.write(soilHumidity);
});

// SLIDER LIVELLO MINIMO UMIDITA'
var minSoilHumidity = 20;
var vMinSoilHumidity = new blynk.VirtualPin(5);
vMinSoilHumidity.on('write', function (param) {
	minSoilHumidity = Number(param[0]);
});

var min = 10000;
var max = 0;
const loop = async() => {
	while (true) {
		getSoilHumidity();
		getDHT11Vals();

//		console.log('AT: ' + airTemperature + 'C, ' + 
//			'AH: ' + airHumidity + '%, ' + 
//			'SH: ' + soilHumidity + '%, ' + 
//			'MSH: ' + minSoilHumidity);
//		if (soilHumidity > max)
//			max = soilHumidity;
//		if (soilHumidity < min)
//			min = soilHumidity;
		if (soilHumidity < minSoilHumidity) {
			pumpOn();
		} else {
			pumpOff();
		}
//		console.log(min + '-' + max);
		await sleep(500);

	}

}

const sleep = (howLong) => {
	return new Promise((resolve) => {
		setTimeout(resolve, howLong);

	})

}

// RELAY

function pumpOn() {
	blynk.virtualWrite(6, 255);
}

function pumpOff() {
	blynk.virtualWrite(6, 0);
}

// Handle Ctrl+C exit cleanly
process.on('SIGINT', () => {
	pumpOff();

	process.exit();

})

loop();
